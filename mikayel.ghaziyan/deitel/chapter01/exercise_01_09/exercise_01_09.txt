Mikayel Ghaziyan
23/09/2017

Exercise 1.9


a.Structural programming a vital part of the whole programming circle. The structural design of the programming makes the production more accurate and precise. It also reduces the risk of structural errors. It is blueprint for the programming process.

b.It includes the definition of the context of the system, designing system architecture, identification of the objects in the system, construction of design models, specification of object interfaces.

c.In 21st century the people mostly exchange digital messages which can include test, graphic, voice, livestream video etc.

d.In modern era, radio components of car have digital interfaces where all controllers are represented in graphics. In addition, there are voice-controlled interfaces which make drivers (person objects) easier to control the radios (objects) while driving. 
