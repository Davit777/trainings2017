We live in a world of objects.Actuall,object technology is a packaging scheme 
that helps us create meaningful software units.Before object-oriented languages appeared,programing languages were focussed on actions rather than on things or objects.With object technology , the software entities created,if properly 
designed,tend to be much more reusable on future projects.C++ programs consist 
of pieces called classes and functions. You can program each piece that you may need to form a C++ program. However, most C++
programmers take advantage of the rich collections of existing classes and 
functions in the C++ Standard Library.
